from random import randint, choice
from PIL import Image, ImageDraw, ImageOps, ImageEnhance
import aggdraw
from math import ceil


class JigsawGenerator(object):
    def __init__(self, src_file_name):
        self.src_file_name = src_file_name
        self.dst_mask = '{}x{}.png'
        self.cols = 1
        self.rows = 1
        self.line_size = 1
        self.background = (0, 0, 0)
        self.foreground = (255, 255, 255)

        self.base_points = [
            {'cx1':0.0,  'cy1':0.0,  'cx2':35.0,'cy2':15.0, 'ex':37.0, 'ey':5.0},   # left shoulder
            {'cx1':37.0, 'cy1':5.0,  'cx2':40.0,'cy2':0.0,  'ex':38.0, 'ey':-5.0},  # left neck
            {'cx1':38.0, 'cy1':-5.0, 'cx2':20.0,'cy2':-20.0,'ex':50.0, 'ey':-20.0}, # left head
            {'cx1':50.0, 'cy1':-20.0,'cx2':80.0,'cy2':-20.0,'ex':62.0, 'ey':-5.0},  # right head
            {'cx1':62.0, 'cy1':-5.0, 'cx2':60.0,'cy2':0.0,  'ex':63.0, 'ey':5.0},   # right neck
            {'cx1':63.0, 'cy1':5.0,  'cx2':65.0,'cy2':15.0, 'ex':100.0,'ey':0.0},   # right shoulder
        ]
        self.w_x = self.base_points[5]['ex']

    def przelicz_wymiary(self, size_from, size_to, points):
        new_points = []
        for p in points:
            p2 = {'cx1':p['cx1']/size_from*size_to,
                  'cy1':p['cy1']/size_from*size_to,
                  'cx2':p['cx2']/size_from*size_to,
                  'cy2':p['cy2']/size_from*size_to,
                  'ex':p['ex']/size_from*size_to,
                  'ey':p['ey']/size_from*size_to}
            new_points.append(p2)
        return new_points

    def points_to_symbol(self, points):
        return self.pathstring_to_symbol(self.points_to_pathstring(points))

    def points_to_pathstring(self, points):
        pathstring = ''
        for p in points:
            #p1 = p['cx1']
            #p2 = p['cy1']
            #pathstring += ' M{},{}'.format(0, 0)

            cx1 = p['cx1']
            cy1 = p['cy1']
            cx2 = p['cx2']
            cy2 = p['cy2']
            ex = p['ex']
            ey = p['ey']
            pathstring += ' C{},{},{},{},{},{}'.format(cx1, cy1, cx2, cy2, ex, ey)
        #pathstring += ' z'
        #print pathstring
        return pathstring

    def points_to_line_pathstring(self, points):
        pathstring = ' L{},{},{},{}'.format(points[0]['cx1'], points[0]['cy1'], points[-1]['ex'], points[-1]['ey'])
        return pathstring

    def pathstring_to_symbol(self, pathstring):
        symbol = aggdraw.Symbol(pathstring)
        return symbol

    def gen_points(self):
        self.w, self.h = self.img.size
        self.w1, self.h1 = self.w/self.cols, self.h/self.rows
        self.points1 = self.przelicz_wymiary(self.w_x, self.w1, self.base_points)

      # odwrocone horyzontalnie
        self.points2 = []
        for p in self.points1:
            p2 = dict(p)
            p2['cy1'] = - p2['cy1']
            p2['cy2'] = - p2['cy2']
            p2['ey'] = - p2['ey']
            self.points2.append(p2)

        # odwrocone 90 stopni
        self.points3 = []
        for p in self.points1:
            p2 = dict(p)
            p2['cx1'] = p['cy1']
            p2['cy1'] = p['cx1']
            p2['cx2'] = p['cy2']
            p2['cy2'] = p['cx2']
            p2['ex'] = p['ey']
            p2['ey'] = p['ex']
            self.points3.append(p2)
        self.points3 = self.przelicz_wymiary(self.w1, self.h1, self.points3)

        self.points4 = []
        for p in self.points2:
            p2 = dict(p)
            p2['cx1'] = p['cy1']
            p2['cy1'] = p['cx1']
            p2['cx2'] = p['cy2']
            p2['cy2'] = p['cx2']
            p2['ex'] = p['ey']
            p2['ey'] = p['ex']
            self.points4.append(p2)
        self.points4 = self.przelicz_wymiary(self.w1, self.h1, self.points4)

        self.sym = [
            self.points_to_symbol(self.points1),
            self.points_to_symbol(self.points2),
            self.points_to_symbol(self.points3),
            self.points_to_symbol(self.points4),
        ]

    def draw_lines(self):
        img = self.img
        draw = aggdraw.Draw(img)
        outline = aggdraw.Pen("black", self.line_size)
        fill = aggdraw.Brush("white")

        for x in range(self.cols):
            for y in range(self.rows-1):
                draw.symbol((x*self.w1,(y+1)*self.h1), self.sym[randint(0,1)], outline)#, fill)

        for x in range(self.cols-1):
            for y in range(self.rows):
                draw.symbol(((x+1)*self.w1,y*self.h1), self.sym[randint(2,3)], outline)#, fill)

        draw.flush()
        #img.save("test.png")
        #img.show()
        #return img

    def gen_1(self):
        outline = aggdraw.Pen(self.foreground, self.line_size)

        margin_x = ceil(self.line_size*0.5) + self.points3[2]['cy2']
        margin_y = ceil(self.line_size*0.5) - self.points1[2]['ey']

        img = Image.new("RGBA", (int(self.w1+margin_x*2), int(self.h1+margin_y*2)), self.background)
        draw = aggdraw.Draw(img)

        draw.symbol((margin_x, margin_y), self.sym[1], outline, self.foreground)
        draw.symbol((margin_x, margin_y), self.sym[3], outline, self.foreground)
        draw.symbol((margin_x+self.w1, margin_y), self.sym[2], outline, self.foreground)
        draw.symbol((margin_x, margin_y+self.h1), self.sym[0], outline, self.foreground)

        draw.flush()

        #draw = ImageDraw.Draw(self.img)
        ImageDraw.floodfill(img, (margin_x+self.w1*0.5, margin_y+self.h1*0.5), self.foreground)

        #img.show()
        img.save('test.png')

    def move_points(self, base_points, x, y):
        points = []
        for l, line in enumerate(base_points):
            new_line = {}
            for p, point in enumerate(line):
                if 'x' in point:
                    new_line[point] = line[point] + x
                else:
                    new_line[point] = line[point] + y
            points.append(new_line)
        return points

    def rotate_points_x(self, base_points):
        #return base_points
        points = []
        for p in base_points:
            p2 = dict(p)
            p2['cx1'] = - p2['cx1']
            p2['cx2'] = - p2['cx2']
            p2['ex'] = - p2['ex']
            points.append(p2)
        return points

    def rotate_points_y(self, base_points):
        #return base_points
        points = []
        for p in base_points:
            p2 = dict(p)
            p2['cy1'] = - p2['cy1']
            p2['cy2'] = - p2['cy2']
            p2['ey'] = - p2['ey']
            points.append(p2)
        return points

    def gen_mask(self, types):
        outline = aggdraw.Pen(self.foreground, self.line_size)

        self.margin_x = ceil(self.line_size*0.5) + self.points3[2]['cy2']
        self.margin_y = ceil(self.line_size*0.5) - self.points1[2]['ey']

        img = Image.new("RGBA", (int(self.w1+self.margin_x*2), int(self.h1+self.margin_y*2)), self.background)
        draw = aggdraw.Draw(img)

        points = []
        pathstring = ''
        type = types[0]
        if type == 0:
            pathstring += self.points_to_line_pathstring(self.points1)
        elif type == 1:
            pathstring += self.points_to_pathstring(self.points1)
        elif type == -1:
            pathstring += self.points_to_pathstring(self.points2)

        type = types[1]
        if type == 0:
            pathstring += self.points_to_line_pathstring(self.move_points(self.points4, self.w1, 0))
        elif type == 1:
            pathstring += self.points_to_pathstring(self.move_points(self.points4, self.w1, 0))
        elif type == -1:
            pathstring += self.points_to_pathstring(self.move_points(self.points3, self.w1, 0))
        type = types[2]
        if type == 0:
            pathstring += self.points_to_line_pathstring(self.move_points(self.rotate_points_x(self.points1), self.w1, self.h1))
        elif type == 1:
            pathstring += self.points_to_pathstring(self.move_points(self.rotate_points_x(self.points2), self.w1, self.h1))
        elif type == -1:
            pathstring += self.points_to_pathstring(self.move_points(self.rotate_points_x(self.points1), self.w1, self.h1))

        type = types[3]
        if type == 0:
            pathstring += self.points_to_line_pathstring(self.move_points(self.rotate_points_y(self.points3), 0, self.h1))
        elif type == 1:
            pathstring += self.points_to_pathstring(self.move_points(self.rotate_points_y(self.points3), 0, self.h1))
        elif type == -1:
            pathstring += self.points_to_pathstring(self.move_points(self.rotate_points_y(self.points4), 0, self.h1))

        symbol = self.pathstring_to_symbol(pathstring)


        draw.symbol((self.margin_x, self.margin_y), symbol, aggdraw.Pen('white', 3), aggdraw.Brush('white'))

        draw.flush()

        #draw = ImageDraw.Draw(self.img)
        #ImageDraw.floodfill(img, (self.margin_x+self.w1*0.5, self.margin_y+self.h1*0.5), self.foreground)

        #img.show()
        #img.save('test.png')
        return img




    def gen_elements(self, cols, rows):
        self.img = Image.open(self.src_file_name)
        self.cols = cols
        self.rows = rows
        self.gen_points()
        #self.gen_1()
        #self.draw_lines()

        elements = []

        for y in range(rows):
            line = []
            for x in range(cols):
                if x == 0:
                    left = 0
                else:
                    left = - line[x-1][1]

                if x == cols-1:
                    right = 0
                else:
                    right = choice([-1, 1])

                if y == 0:
                    top = 0
                else:
                    top = - elements[y-1][x][2]

                if y == rows-1:
                    bottom = 0
                else:
                    bottom = choice([-1, 1])

                line.append([top, right, bottom, left])
            elements.append(line)

        for i, row in enumerate(elements):
            for j, element in enumerate(elements[i]):
                mask = self.gen_mask(element).convert('L')
                #mask.show()
                img = Image.new("RGBA", mask.size, self.background)
                offset = (int(self.margin_x - self.w1 * j), int(self.margin_y - self.h1 * i))
                img.paste(self.img, offset)
                output = ImageOps.fit(img, mask.size, centering=(0.5, 0.5))
                output.putalpha(mask)
                output.save(self.dst_mask.format(i, j))

    def set_brightness(self, img, factor):
        enhancer = ImageEnhance.Contrast(img)
        ret = enhancer.enhance(0.1)
        enhancer = ImageEnhance.Brightness(ret)
        ret = enhancer.enhance(1.3)
        return ret


if __name__ == '__main__':
    src_file_name = 'img11_m.png'
    img = JigsawGenerator(src_file_name)
    img.dst_mask = 'img11_m_{}_{}.png'
    img.line_size = 1
    img.gen_elements(3, 2)
    img.draw_lines()
    img.img = img.set_brightness(img.img, 3.0)
    img.img.save('img11_m_grid.png')
