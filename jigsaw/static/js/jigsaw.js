
JigsawElement = function(parent, x, y) {
  this.parent = parent;
  this.object = null;

  this.create_element = function(x, y) {
    //alert(this.parent.name);
    //alert(x + ' ' + y);
    this.object = $('<img id="el_'+x+'_'+y+'" class="element" src="static/img/img2_m_'+x+'_'+y+'.png">')
    this.object.appendTo(this.parent.object);
  };

  this.create_element(x, y);
};

JigsawBoard = function(parent) {
  this.top_z = 0;
  this.parent = $(parent);
  this.object = null;
  this.grid_obj = null;
  this.name = '';
  this.rows = 0;
  this.cols = 0;
  this.elements = [];

  this.clear_elements = function() {
    this.elements = [];
  };

  this.create_elements = function(cols, rows) {
    for (var j = 0; j < rows; j++) {
      for (var i = 0; i < cols; i++) {
        var el = new JigsawElement(this, j, i);
        this.elements.push(el);
      };
    };
  };

  this.set_img = function(name, width, height, left, top, cols, rows) {
    this.rows = rows;
	this.cols = cols;
    this.grid_obj.css('background-image', 'url("static/img/'+name+'_grid.png")');
    this.grid_obj.width(width);
    this.grid_obj.height(height);
    this.grid_obj.css('left', left);
    this.grid_obj.css('top', top);
    this.clear_elements();
    this.create_elements(cols, rows);

  };

  this.create_board = function() {
	this.object = $('<div class="board"></div>');
	this.object.appendTo(this.parent);

    this.grid_obj = $( '<div class="grid"></div>')
    this.grid_obj.appendTo(this.object);
  };
  
  this.create_board();
};




var topZ = 0;

$(function() {
  board = new JigsawBoard('body');
  board.set_img('img2_m', 750, 422, 60, 70, 3, 2);
  /*
  
    $(".element").draggable({
      containment: ".board", 
      scroll: false ,
      start: function() {
        topZ += 1;
        $(this).css('zIndex', topZ);
      }
    });

    $(".element").hover(function() {
      $(this).toggleClass('shadow');
    });

    $(".element").click(function() {
      topZ += 1;
      $(this).css('zIndex', topZ);
    });
   */
});
